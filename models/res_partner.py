# -*- coding: utf-8 -*-

from odoo import models, fields, api

# class extended_auth(models.Model):
#     _name = 'extended_auth.extended_auth'

    # name = fields.Char()
    # value = fields.Integer()
    # value2 = fields.Float(compute="_value_pc", store=True)
    # description = fields.Text()

    # @api.depends('value')
    # def _value_pc(self):
    #     self.value2 = float(self.value) / 100

class Partner(models.Model):
    _inherit = "res.partner"

    cnpj_cpf = fields.Char(string="CNPJ/CPF", size=18)
    education = fields.Char(string="Escolaridade")
    birthdate = fields.Char(string="Data de Nascimento")
    gender = fields.Char(string="Gênero")
