# -*- coding: utf-8 -*-
from odoo import http

class ExtendedAuth(http.Controller):
    @http.route('/extended_auth/extended_auth/', auth='public')
    def index(self, **kw):
        return "Hello, world"

    @http.route('/extended_auth/extended_auth/objects/', auth='public')
    def list(self, **kw):
        return http.request.render('extended_auth.listing', {
            'root': '/extended_auth/extended_auth',
            'objects': http.request.env['extended_auth.extended_auth'].search([]),
        })

    @http.route('/extended_auth/extended_auth/objects/<model("extended_auth.extended_auth"):obj>/', auth='public')
    def object(self, obj, **kw):
        return http.request.render('extended_auth.object', {
            'object': obj
        })

    @http.route('/web/signup', type='http', auth='public', website=True, sitemap=False)
    def web_auth_signup(self, *args, **kw):
        #qcontext = self.get_auth_signup_qcontext()

        # if not qcontext.get('token') and not qcontext.get('signup_enabled'):
        #     raise werkzeug.exceptions.NotFound()

        # if 'error' not in qcontext and request.httprequest.method == 'POST':
        #     try:
        #         self.do_signup(qcontext)
        #         # Send an account creation confirmation email
        #         if qcontext.get('token'):
        #             user_sudo = request.env['res.users'].sudo().search([('login', '=', qcontext.get('login'))])
        #             template = request.env.ref('auth_signup.mail_template_user_signup_account_created', raise_if_not_found=False)
        #             if user_sudo and template:
        #                 template.sudo().with_context(
        #                     lang=user_sudo.lang,
        #                     auth_login=werkzeug.url_encode({'auth_login': user_sudo.email}),
        #                 ).send_mail(user_sudo.id, force_send=True)
        #         return self.web_login(*args, **kw)
        #     except UserError as e:
        #         qcontext['error'] = e.name or e.value
        #     except (SignupError, AssertionError) as e:
        #         if request.env["res.users"].sudo().search([("login", "=", qcontext.get("login"))]):
        #             qcontext["error"] = _("Another user is already registered using this email address.")
        #         else:
        #             _logger.error("%s", e)
        #             qcontext['error'] = _("Could not create a new account.")

        response = request.render('extended_auth_signup.fields', qcontext)
        response.headers['X-Frame-Options'] = 'DENY'
        return response